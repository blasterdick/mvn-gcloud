Docker image for using as a maven build container with GitLab CI Google App Engine Flex pipelines.

Based on maven:jdk-8-alpine with instructions for getting google-cloud-sdk & components installed.

An example of `stage` for GitLab CI 9.X `.gitlab-ci.yml`:


```
#!shell

stages:
  - build

mvn-build-deploy-app:
  image: bicollect/mvn-gcloud:latest
  stage: build
  variables:
    MAVEN_OPTS: -Dmaven.repo.local=/cache/maven.repository
  only:
    - tp-81685
    - /^[0-9].*$/
  except:
    - /^[a-zA-Z].*$/
  before_script:
    - echo "$GOOGLE_KEY" > key.json
  script:
    - gcloud auth activate-service-account --key-file key.json
    - gcloud config set compute/zone europe-west1-b
    - gcloud config set project $GOOGLE_PROJECT_NAME
    - mvn clean package -B --settings .m2/settings.xml
    - ./mvn.deploy.exp $CI_COMMIT_TAG
  artifacts:
    paths:
      - target/*.jar
```


`$GOOGLE_KEY` and `$GOOGLE_PROJECT_NAME` variables stands for project Secret Variables, configured on the GitLab CI/CD pipelines settings page.


Expect example script (for do not wait for default GAE service to be updated):

```
#!shell

#!/usr/bin/expect

set VERSION [lindex $argv 0]

set timeout -1

spawn mvn appengine:deploy -Dapp.deploy.version=$VERSION -Dapp.deploy.promote=false

expect "Updating service"

send \x03

```

Place it in a root of a project.
